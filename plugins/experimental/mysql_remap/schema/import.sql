/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;

--
-- Table structure for table `map`
--

DROP TABLE IF EXISTS `map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_scheme` varchar(5) NOT NULL,
  `from_hostname` varchar(255) NOT NULL,
  `from_port` int(5) unsigned NOT NULL,
  `to_scheme` varchar(5) NOT NULL,
  `to_hostname` varchar(255) NOT NULL,
  `to_port` int(5) unsigned NOT NULL DEFAULT '80',
  `is_enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `from_unique` (`from_scheme`,`from_hostname`,`from_port`),
  KEY `to_hostname` (`to_hostname`),
  KEY `from_hostname` (`from_hostname`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `map`
--

LOCK TABLES `map` WRITE;
/*!40000 ALTER TABLE `map` DISABLE KEYS */;
INSERT INTO `map` VALUES (1,'http','ts.g-cdn.net',80,'http','go.app1pro.com',80,1);
/*!40000 ALTER TABLE `map` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;
